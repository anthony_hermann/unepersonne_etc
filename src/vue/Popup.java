package vue;

import javax.swing.JOptionPane;

public class Popup implements Affiche {

	private JOptionPane popup;

	public Popup() {
		this.popup = new JOptionPane();
	}

	@Override
	public int saisir() {

		return 0;
	}

	@SuppressWarnings("static-access")
	@Override
	public String saisir(String msg) {
		String nom;
		do {
			nom = this.popup.showInputDialog(null, msg, "Saisie", JOptionPane.QUESTION_MESSAGE);
			if (nom == null) {
				System.exit(0);
			}
		} while (nom.isEmpty());

		return nom;
	}

	@SuppressWarnings("static-access")
	@Override
	public void afficher(String s) {
		this.popup.showMessageDialog(null, s, "Affiche", JOptionPane.INFORMATION_MESSAGE);

	}

	@Override
	public void afficherLn(String s) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("static-access")
	@Override
	public int saisirInt(String msg) {
		int age;
		String ageString = "";
		boolean ok = false;
		do {
			try {
				// age = Integer.parseInt
				ageString = this.popup.showInputDialog(null, msg, "Saisie", JOptionPane.QUESTION_MESSAGE);
				if (Integer.parseInt(ageString) < 121 && Integer.parseInt(ageString) >= 0 ) {

					ok = true;
				}
				
			} catch (Exception e) {
				

			}
			 if (ageString == null) {
				System.exit(0);
			}
		} while (ageString.isEmpty() || ok == false);

		age = Integer.parseInt(ageString);
		return age;
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean afficheTab(String msg) {
		boolean vote = false;
		String[] Liste = { "oui", "non" };
		String resultat;

		resultat = (String) this.popup.showInputDialog(null, msg, "Saisie", JOptionPane.QUESTION_MESSAGE, null, Liste,
				Liste[1]);
		if (resultat == "oui") {
			vote = true;
		} else if (resultat == null) {
			System.exit(0);
		}

		return vote;
	}

}
