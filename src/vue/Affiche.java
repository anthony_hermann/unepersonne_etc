package vue;

public interface Affiche {
	
	public int saisir();
	public String saisir(String msg);
	public void afficher(String s);
	public void afficherLn(String s);
	public int saisirInt(String msg);
	public boolean afficheTab(String msg);
}
