package entities;

public class Majeur extends Personne {

	
	private boolean vote;
	
	// constructeurs
	public Majeur() {
		
	}
	
	public Majeur(String xNom, String xPrenom, int xAge,boolean xVote) {
		super(xNom, xPrenom, xAge);
		this.setVote(xVote);
	}

	// methodes 
	

	// Accesseurs
	
	public boolean isVote() {
		return vote;
	}

	public void setVote(boolean xVote) {
			this.vote = xVote;

		
		
	}
	
	public String toString() {
		return super.toString() + " " + (this.isVote() ? "vote" : "ne vote pas");
	}

}
