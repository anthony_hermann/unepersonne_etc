package entities;

import tools.Format;

public class Personne {

	
	// Propri�t�
	
	private String nom;
	private String prenom;
	private int age;
	
	
	// Constructeur
	
	public Personne() {
		
	}
	
	public Personne(String xNom,String xPrenom,int xAge) {
		this.setNom(xNom);
		this.setPrenom(xPrenom);
		this.setAge(xAge);
		
	}


	// ACCESSEURS getset transformer les parametres priver en public en passant par une methode
	public String getNom() {
		return nom;
	}


	public void setNom(String leNom) {
		this.nom = Format.majuscule(leNom);
	}


	public String getPrenom() {
		return Format.premiereLettreEnMaj(prenom);
	}


	public void setPrenom(String lePrenom) {
		this.prenom = lePrenom;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int l_Age) {
		this.age = l_Age;
	}
	
	// METHODE TOSTRING creer une string recup�rant toute les methodes des get des param�tres pour concatenation
	
	public String toString() {
		return this.getNom() + " " + this.getPrenom() + " " + this.getAge() + " ans";
	}

}

