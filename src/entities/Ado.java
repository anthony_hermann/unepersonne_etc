package entities;

public class Ado extends Mineur {
	
	//propri�t�s
	
	private boolean portable;
	private String ecole;

	public Ado(String xNom, String xPrenom, int xAge, String xEcole,boolean xPortable) {
		super(xNom, xPrenom, xAge);
		this.setPortable(xPortable);
		this.setEcole(xEcole);
		
	}

	public boolean isPortable() {
		return portable;
	}

	public void setPortable(boolean xPortable) {
		this.portable = xPortable;
	}
	
	public String getEcole() {
		return ecole;
	}

	public void setEcole(String ecole) {
		this.ecole = ecole;
	}

	// methodes
	
	public String toString() {
		return super.toString() + " " + (this.isPortable() ? "j'ai un portable" : "je n'ai pas de portable") + " " + "Mon �cole est : "+this.getEcole() ;
	
	}

	
}
