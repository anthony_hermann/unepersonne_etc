package entities;

public class Enfant  extends Mineur{
	
	// propri�t�s
	
	private boolean velo;
	private String ecole;
	
	

	public Enfant(String xNom, String xPrenom, int xAge, String xEcole,boolean xVelo) {
		super(xNom, xPrenom, xAge);
		this.setVelo(xVelo);
		this.setEcole(xEcole);
	}



	public boolean isVelo() {
		return velo;
	}



	public void setVelo(boolean xVelo) {
		this.velo = xVelo;
	}
	
	public String getEcole() {
		return ecole;
	}



	public void setEcole(String ecole) {
		this.ecole = ecole;
	}
	
	// methodes
		public String toString() {
			return super.toString() + " " + (this.isVelo() ? "j'ai un v�lo" : "je n'ai pas de v�lo") + " " + "Mon �cole est : "+ this.getEcole() ;
		}



		

}
