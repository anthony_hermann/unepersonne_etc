package projetVote;


import entities.Ado;
import entities.Enfant;
import entities.Majeur;
import entities.Personne;
import vue.Popup;

public class ProjetMain {

	public static void main(String[] args) {
		
		Popup pop = new Popup();
		
		//instance de Personne
		Personne unePersonne = new Personne();
		unePersonne.setNom(pop.saisir("Saisir votre nom : "));				
		unePersonne.setPrenom(pop.saisir("Saisir votre pr�nom : "));
		unePersonne.setAge(pop.saisirInt("Saisir votre Age"));
		
		if (unePersonne.getAge() >= 18) {
			
		
		// instance de Majeur
		Majeur maj = new Majeur(unePersonne.getNom(),unePersonne.getPrenom(),unePersonne.getAge(),true);
		//pop.afficher(maj.toString());
		
		maj.setVote(pop.afficheTab("Votez-vous ? "));
		pop.afficher(maj.toString());
		}
		
		
		if(unePersonne.getAge() < 12) {
			
		Enfant enf = new Enfant(unePersonne.getNom(),unePersonne.getPrenom(),unePersonne.getAge(),"",true);
		enf.setEcole(pop.saisir("Saisir votre ecole : "));
		enf.setVelo(pop.afficheTab("Avez-vous un Velo ? "));
		pop.afficher(enf.toString());
		}
		
		else if (unePersonne.getAge() >= 12 && unePersonne.getAge() < 18) {
		Ado adol = new Ado(unePersonne.getNom(),unePersonne.getPrenom(),unePersonne.getAge(),"",true);
		adol.setEcole(pop.saisir("Saisir votre ecole : "));
		adol.setPortable(pop.afficheTab("Avez vous un portable ?"));
		pop.afficher(adol.toString());
		}
		//instance de mineur
		
		

	}
		
		

}
